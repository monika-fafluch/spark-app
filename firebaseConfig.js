
import firebase from 'firebase'
import 'firebase/firestore'


const config = {
    apiKey: "AIzaSyAbyrGfOqU8WNilkVqSKzTFbkgBfojRFDk",
    authDomain: "spark-b8aa4.firebaseapp.com",
    databaseURL: "https://spark-b8aa4.firebaseio.com",
    projectId: "spark-b8aa4",
    storageBucket: "spark-b8aa4.appspot.com",
    messagingSenderId: "541062449533",
    appId: "1:541062449533:web:485f413258a74fc00c2414",
    measurementId: "G-TWK03SJ8K6"
}
firebase.initializeApp(config)

const db = firebase.firestore()
const auth = firebase.auth()


export {
    db,
    auth
}