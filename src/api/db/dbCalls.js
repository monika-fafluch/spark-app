import router from '../../router/index'
import { store } from '../../store/store'

const firebase = require('../../../firebaseConfig.js')
const auth = firebase.auth
const db = firebase.db
// const userId = store.state.loggedUser.uid

export default {
  signUp (email, password, username) {
    return firebase.auth.createUserWithEmailAndPassword(email, password)
      .then(function (result) {
        router.push({name: 'LoginForm'})
        return result.user.updateProfile({
          displayName: username
        })
      })
      .catch(function (error) {
        console.log(error)
      })
  },
  logIn (email, password) {
    return firebase.auth.signInWithEmailAndPassword(email, password).then(function () {
      store.commit('setCurrentUser', auth.currentUser)
      router.push({ name: 'HomePage' })
    })
  },
  logOut () {
    return firebase.auth.signOut()
  },
  fetchQuotes (userId) {
    return db.collection('users').doc(userId).collection('quotes').orderBy('created', 'desc').get()
  },
  addQuote (quote, author, userId) {
    return db.collection('users').doc(userId).collection('quotes').add({
      quote: quote,
      author: author,
      created: Date.now()
    })
  },
  deleteQuote (quoteId, userId) {
    return db.collection('users').doc(userId).collection('quotes').doc(quoteId).delete()
  }
}
