import $ from 'jquery'

export default {
  getRandomQuote (url) {
    return $.ajax({
      type: 'GET',
      url: url,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', 'Token ece0647e23bd606ef68c8d436bd2f0d404f54b4a')
      }
    })
  },
  getQuotesTags () {
    return $.ajax({
      type: 'GET',
      url: 'https://api.paperquotes.com/apiv1/tags/?limit=15',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', 'Token ece0647e23bd606ef68c8d436bd2f0d404f54b4a')
      }
    })
  }
}
