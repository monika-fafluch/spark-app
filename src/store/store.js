import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    loggedUser: '',
    categories: []
  },
  mutations: {
    setCurrentUser (state, val) {
      state.loggedUser = val
    },
    setCategories (state, val) {
      state.categories.push(val)
    },
    resetCategories (state) {
      state.categories = []
    }
  },
  getters: {
    loggedUser: state => state.loggedUser,
    categories: state => state.categories
  },
  plugins: [createPersistedState()]
})
