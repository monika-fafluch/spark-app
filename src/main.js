// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
// router
import router from './router'
// store
import { store } from './store/store'
// vue-mq for responsive design
import VueMq from 'vue-mq'
// copy to clipboard
import Clipboard from 'v-clipboard'
// bootstrap and bootstrap-vue
import { BootstrapVue } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// font awesome
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
// vuelidate
import Vuelidate from 'vuelidate'

const fb = require('../firebaseConfig.js')

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(Clipboard)
Vue.use(Vuelidate)

Vue.use(VueMq, {
  breakpoints: {
    mobile: 450,
    tablet: 900,
    laptop: 1250,
    desktop: Infinity
  }
})

let app
fb.auth.onAuthStateChanged(user => {
  if (!app) {
    app = new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App)
    })
  }
})
