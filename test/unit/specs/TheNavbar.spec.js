import { mount } from '@vue/test-utils'
import TheNavbar from '@/components/TheNavbar'
import { createLocalVue } from '@vue/test-utils'
import { BootstrapVue } from 'bootstrap-vue'

let wrapper

const localVue = createLocalVue()
localVue.use(BootstrapVue)

const mockStore = {
  state: {
    loggedUser: {
      uid: 'FRumxBqY37'
    }
  }
}

beforeEach(() => {
  wrapper = mount(TheNavbar, {
    localVue,
    mocks: {
      $store: mockStore
    }
  })
})

afterEach(() => {
  wrapper.destroy()
})


describe('TheNavbar', () => {

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
  test('home nav item is present', () => {
    expect(wrapper.text()).toMatch('Home')
  })
  test('settings nav item is present', () => {
    expect(wrapper.text()).toMatch('Settings')
  })
  test('logout nav item is present', () => {
    expect(wrapper.text()).toMatch('Log Out')
  })
  it('is named correctly', () => {
    expect(wrapper.name()).toMatch('TheNavbar')
  })
})
