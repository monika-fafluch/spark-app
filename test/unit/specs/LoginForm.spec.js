import { mount } from '@vue/test-utils'
import LoginForm from '@/components/LoginForm'
import { createLocalVue } from '@vue/test-utils'
import { BootstrapVue } from 'bootstrap-vue'
import Vuelidate from 'vuelidate'

let wrapper

const localVue = createLocalVue()
localVue.use(BootstrapVue)
localVue.use(Vuelidate)

const mockStore = {
  state: { 
    loggedUser: {
      uid: 'FRumxBqY37'
    },
    categories: ['example1', 'example2']
  }
}

beforeEach(() => {
  wrapper = mount(LoginForm, {
    localVue,
    mocks: {
      $store: mockStore
    }
  })
})

afterEach(() => {
  wrapper.destroy()
})


describe('LoginForm', () => {

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
  it('is named correctly', () => {
    expect(wrapper.name()).toMatch('LoginForm')
  })
  it('has data', () => {
    expect(typeof LoginForm.data).toBe('function')
  })
  it('sets the correct default data', () => {
    expect(typeof LoginForm.data).toBe('function')
  })
  it('sets the correct default data', () => {
    const defaultData = LoginForm.data()
    expect(defaultData.email).toBe('')
    expect(defaultData.password).toBe('')
  })
  it("checks quote form submittion", async () => {
    const componentData = LoginForm.data()
    const inputEmail = wrapper.find(".form-input-email")
    const inputPassword = wrapper.find(".form-input-password")
    inputEmail.setValue("monika")
    inputPassword.setValue("password")
    wrapper.find("form").trigger("submit.prevent")
    wrapper.find("button").element.click()
    await wrapper.vm.$nextTick()

    expect(inputEmail.element.value).toBe('monika')
    expect(inputPassword.element.value).toBe('password')
  })

})
