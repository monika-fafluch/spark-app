import { mount } from '@vue/test-utils'
import HomePage from '@/components/HomePage'
import { createLocalVue } from '@vue/test-utils'
import { BootstrapVue, BListGroup } from 'bootstrap-vue'

let wrapper

const localVue = createLocalVue()
localVue.use(BootstrapVue)

const mockStore = { 
  state: {
    loggedUser: {
      uid: 'FRumxBqY37'
    },
    categories: ['example1', 'example2']
  }
}

beforeEach(() => {
  wrapper = mount(HomePage, {
    localVue,
    mocks: {
      $store: mockStore
    }
  })
})

afterEach(() => {
  wrapper.destroy()
})


describe('HomePage', () => {

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('is named correctly', () => {
    expect(wrapper.name()).toMatch('HomePage')
  })

  it('has data', () => {
    expect(typeof HomePage.data).toBe('function')
  })

  it('has a created hook', () => {
    expect(typeof HomePage.created).toBe('function')
  })

  it('has a mounted hook', () => {
    expect(typeof HomePage.mounted).toBe('function')
  })
  it('sets the correct default data', () => {
    const defaultData = HomePage.data()
    expect(defaultData.quote).toBe('')
    expect(defaultData.author).toBe('')
    expect(defaultData.keywords).toBe('')
    expect(defaultData.random).toBe('')
    expect(defaultData.quotes).toEqual([])
    expect(defaultData.quoteAddedFlag).toBe(false)
    expect(defaultData.quoteDeletedFlag).toBe(false)
    expect(defaultData.showRandomModal).toBe(false)
  })

  it("checks quote form submittion", async () => {
    const componentData = HomePage.data()
    const inputQuote = wrapper.find(".form-input-quote")
    const inputAuthor = wrapper.find(".form-input-author")
    const inputSearching = wrapper.find(".input-searching")
    inputQuote.setValue("my quote")
    inputAuthor.setValue("author")
    inputSearching.setValue("einstein's quote")
    wrapper.find("form").trigger("submit.prevent")
    wrapper.find("button").element.click()
    await wrapper.vm.$nextTick()

    expect(inputQuote.element.value).toBe("my quote")
    expect(inputAuthor.element.value).toBe("author")
    expect(inputSearching.element.value).toBe("einstein's quote")
  })

  it("checks if quotes list exists", () => {
    expect(wrapper.find(BListGroup).exists()).toBe(true)
  })

})
