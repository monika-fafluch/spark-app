import { mount } from '@vue/test-utils'
import SignUpForm from '@/components/SignUpForm'
import { createLocalVue } from '@vue/test-utils'
import { BootstrapVue } from 'bootstrap-vue'
import Vuelidate from 'vuelidate'

let wrapper

const localVue = createLocalVue()
localVue.use(BootstrapVue)
localVue.use(Vuelidate)

const mockStore = {
	state: {
		loggedUser: {
			uid: 'FRumxBqY37'
		}
	}
}

beforeEach(() => {
	wrapper = mount(SignUpForm, {
    localVue,
		mocks: {
    		$store: mockStore
    }
	})
})

afterEach(() => {
	wrapper.destroy()
})


describe('SignUpForm', () => {

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
  it('is named correctly', () => {
    expect(wrapper.name()).toMatch('SignUpForm')
  })
  it('has data', () => {
    expect(typeof SignUpForm.data).toBe('function')
  })
  it('sets the correct default data', () => {
    expect(typeof SignUpForm.data).toBe('function')
  })
  it('sets the correct default data', () => {
    const defaultData = SignUpForm.data()
    expect(defaultData.username).toBe('')
    expect(defaultData.email).toBe('')
    expect(defaultData.password1).toBe('')
    expect(defaultData.password2).toBe('')
  })
  it("checks quote form submittion", async () => {
    const componentData = SignUpForm.data()
    const inputUsername = wrapper.find(".form-input-username")
    const inputEmail = wrapper.find(".form-input-email")
    const inputPassword1 = wrapper.find(".form-input-password1")
    const inputPassword2 = wrapper.find(".form-input-password2")
    inputUsername.setValue("monika")
    inputEmail.setValue("me@me.com")
    inputPassword1.setValue("password")
    inputPassword2.setValue("password")
    wrapper.find("form").trigger("submit.prevent")
    wrapper.find("button").element.click()
    await wrapper.vm.$nextTick()

    expect(inputUsername.element.value).toBe('monika')
    expect(inputEmail.element.value).toBe('me@me.com')
    expect(inputPassword1.element.value).toBe('password')
    expect(inputPassword2.element.value).toBe('password')
    expect(inputPassword1.element.value).toBe(inputPassword2.element.value)
  })

})
