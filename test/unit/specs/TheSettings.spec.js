import { mount } from '@vue/test-utils'
import TheSettings from '@/components/TheSettings'
import { createLocalVue } from '@vue/test-utils'
import { BootstrapVue } from 'bootstrap-vue'

let wrapper

const localVue = createLocalVue()
localVue.use(BootstrapVue)

const mockStore = { 
  state: {
    loggedUser: {
      uid: 'FRumxBqY37'
    },
    categories: ['example1', 'example2']
  }
}

beforeEach(() => {
  wrapper = mount(TheSettings, {
    localVue,
    mocks: {
      $store: mockStore
    }
  })
})

afterEach(() => {
  wrapper.destroy()
})


describe('TheSettings', () => {

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('is named correctly', () => {
    expect(wrapper.name()).toMatch('TheSettings')
  })

  it('has data', () => {
    expect(typeof TheSettings.data).toBe('function')
  })

  it('has a created hook', () => {
    expect(typeof TheSettings.created).toBe('function')
  })


  it('sets the correct default data', () => {
    const defaultData = TheSettings.data()
    expect(defaultData.selected).toEqual([])
    expect(defaultData.options).toEqual([])
  })

})
